using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace final
{
    class Final
    {
        private string[] empleados;
        private int[,] sueldos;
        private int[] sueldostot;

        public void Cargar()
        {
            empleados = new String[5];
            sueldos = new int[5, 3];
            for (int f = 0; f < empleados.Length; f++)
            {
                Console.Write("Inserte el nombre del empleado " + (f + 1) + ": ");
                empleados[f] = Console.ReadLine();
                for (int c = 0; c < sueldos.GetLength(1); c++)
                {
                    Console.Write("Ingrese sueldo " + (c + 1) + ": ");
                    string linea;
                    linea = Console.ReadLine();
                    sueldos[f, c] = int.Parse(linea);
                }
            }
        }

        public void CalcularSumaSueldos()
        {
            sueldostot = new int[5];
            for (int f = 0; f < sueldos.GetLength(0); f++)
            {
                int suma = 0;
                for (int c = 0; c < sueldos.GetLength(1); c++)
                {
                    suma = suma + sueldos[f, c];
                }
                sueldostot[f] = suma;
            }
        }

        public void ImprimirTotalPagado()
        {
            Console.WriteLine("Total de sueldos pagados por Empleado.");
            for (int f = 0; f < sueldostot.Length; f++)
            {
                Console.WriteLine(empleados[f] + " - " + sueldostot[f]);
            }
        }

        public void EmpleadoMayorSueldo()
        {
            int mayor = sueldostot[0];
            string nom = empleados[0];
            for (int f = 0; f < sueldostot.Length; f++)
            {
                if (sueldostot[f] > mayor)
                {
                    mayor = sueldostot[f];
                    nom = empleados[f];
                }
            }
            Console.WriteLine("El empleado con mayor sueldo es: " + nom + " y tiene un sueldo de: " + mayor);
        }

        static void Main(string[] args)
        {
            Final emple = new Final();
            emple.Cargar();
            emple.CalcularSumaSueldos();
            emple.ImprimirTotalPagado();
            emple.EmpleadoMayorSueldo();
            Console.ReadKey();
        }
    }
}
